"use strict";

/**
 * @param {string} s
 * @return {number}
 */
var lengthOfLongestSubstring = function (s) {
  let sub = {};
  let oldcount = 0;
  let newcount = 0;

  for (let i = 0; i < s.length; i++) {
    if (s[i] in sub) {
      if (newcount > oldcount) oldcount = newcount;
      i = sub[s[i]];
      sub = {};
      newcount = 0;
      continue;
    }
    newcount++;
    sub[s[i]] = i;
  }
  return oldcount > newcount ? oldcount : newcount;
};

console.log(lengthOfLongestSubstring("abcabcbb"));
console.log(lengthOfLongestSubstring("bbbbb"));
console.log(lengthOfLongestSubstring("pwwkew"));
console.log(lengthOfLongestSubstring("dvdf"));
console.log(lengthOfLongestSubstring("dvldf"));
console.log(lengthOfLongestSubstring(" "));
console.log(lengthOfLongestSubstring(""));

// Let see who does it work
// "abcabcbb" => "abc" => 3
// "bbbbb" => "b" => 1
// "pwwkew" => "wke" => 3
// "dvdf" => "vdf" => 3
// "dvldf" => "vldf" => 4

/*
    for this example "pw wkew"
    get the index of the first and last char
    sub {"p": 0, "w": 1} we found another w so,
                    we set indices to [0, 1] and emtpy the the object
                    oldcount = 2;
    sub {"w": 2, "k": 3, "e": 4}
                    newcount > oldcount
                        we set indices to [2, 4] and empty the object to continue
                    oldcount = newcount;
                    newcount = 0;
    sub {"w": 5} and we reached the end of the string
                return substring newcount

*/
