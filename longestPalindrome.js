const fs = require("fs");

//mom;
//sjsolos 0 , 6
var isPalindrom = function (s, i, len) {
  while (s[i] === s[len]) {
    if (len <= i) return true;
    i++;
    len--;
  }
  return false;
};

var readfile = (filename) => {
  const data = fs.readFileSync(filename);
  return data.toString().split("\n");
};

/**
 * @param {string} s
 * @return {string}
 */
var longestPalindrome = function (s) {
  let indices = [0, 1];
  for (let i = 0; i < s.length; i++) {
    for (let j = i; j < s.length; j++) {
      if (isPalindrom(s, i, j)) {
        if (j - i >= indices[1] - indices[0]) indices = [i, j + 1];
      }
    }
  }
  console.log(indices, s.substring(...indices));
  return s.substring(...indices);
};

/*
  oatcivickarim => civic
    o - m => NO
    o - i => No
    o - r => NO
      ... 
    o - o => Yes
      ...
    a - m

*/

const longestPalindrome2 = (s) => {
  let i = 0,
    j = 0;
  let indices = [0, 1];
  while (j < s.length) {
    if (isPalindrom(s, i, j)) {
      if (j - i >= indices[1] - indices[0]) indices = [i, j + 1];
    }
    if (j === s.length - 1) {
      i++;
      j = i;
    }
    if (i === s.length - 1) break;
    j++;
  }
  return s.substring(...indices);
};

function checkfile() {
  words = readfile("./palindrome");

  words.forEach((word) => {
    word = word.substr(1, word.length - 1);
    console.log(longestPalindrome2(word.toLowerCase()));
  });
}

if (isPalindrom("sjsolos", 0, 6)) console.log("Hello, ", "sjsolos");
checkfile();
