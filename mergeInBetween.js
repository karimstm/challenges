function ListNode(val, next) {
    this.val = val === undefined ? 0 : val;
    this.next = next === undefined ? null : next;
  }
  
  function insertToList(arr) {
    let l1 = null;
    for (let i = arr.length - 1; i >= 0; i--) {
      l1 = new ListNode(arr[i], l1);
    }
    return l1;
  }

  /**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} list1
 * @param {number} a
 * @param {number} b
 * @param {ListNode} list2
 * @return {ListNode}
 */
var mergeInBetween = function(list1, a, b, list2) {
    let list1_copy = list1;
    let list2_copy = list2;
    let prev = null;
    let next = null;
    let counter = 0;

    while (list2_copy)
    {
        if (list2_copy.next === null)
            break;
        list2_copy = list2_copy.next;
    }

    while (list1_copy)
    {
        if (a - 1 === counter)
            prev = list1_copy;
        if (counter === b)
        {
            next = list1_copy;
            break;
        }
        list1_copy = list1_copy.next;
        counter++;
    }
    if (prev)
        prev.next = list2;
    if (list2_copy)
        list2_copy.next = next;
    return list1;
};



let list1 = insertToList([0,1,2,3,4,5]);
let list2 = insertToList([1000000,1000001,1000002]);

console.log(JSON.stringify(list1))

mergeInBetween(list1, 3, 4, list2);