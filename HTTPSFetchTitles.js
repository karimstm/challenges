"use strict";

const https = require("https");

const URL = "https://jsonmock.hackerrank.com/api/articles";

const fetchData = async (author, page) =>
  new Promise((resolve, reject) => {
    https.get(`${URL}?author=${author}&page=${page}`, (res) => {
      const { statusCode } = res;
      const contentType = res.headers["content-type"];

      let error;
      if (statusCode !== 200) {
        error = new Error("Request Failed.\n" + `Status code: ${statusCode}`);
      } else if (!/^application\/json/.test(contentType)) {
        error = new Error(
          "Invalid content-type.\n" +
            `Expected application/json but received ${contentType}`
        );
      }

      if (error) {
        console.log(error.message);
        // Consume response data to free up memory
        res.resume();
        return;
      }

      res.setEncoding("utf-8");
      let rawData = "";
      res.on("data", (chunk) => {
        rawData += chunk;
      });
      res
        .on("end", () => {
          const parsedData = JSON.parse(rawData);
          resolve(parsedData);
        })
        .on("error", (e) => {
          reject(e.message);
        });
    });
  });

async function getArticleTitles(author) {
  let titles = []; // All titles will be stored in this array
  let total_pages; // Number of pages in response
  let page_number; // Number of the current page
  let AllPages = []; // Store promises in this array

  const response = await fetchData(author, 1);
  total_pages = parseInt(response.total_pages);
  page_number = parseInt(response.page);
  titles = getTitlesFromResponse(response);
  for (
    let current_page = page_number + 1;
    current_page <= total_pages;
    current_page++
  ) {
    AllPages.push(fetchData(author, current_page));
  }
  let data = await Promise.all(AllPages);
  data.forEach((p) => {
    titles.push(...getTitlesFromResponse(p));
  });
  return titles;
}

function getTitlesFromResponse(res) {
  let titles = [];
  res.data.forEach((art) => {
    if (art.title) return titles.push(art.title);
    else if (art.story_url) return titles.push(art.story_url);
  });
  return titles;
}

getArticleTitles("coloneltcb").then((r) => console.log(r));
