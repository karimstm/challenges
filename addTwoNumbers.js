function ListNode(val, next) {
  this.val = val === undefined ? 0 : val;
  this.next = next === undefined ? null : next;
}

function insertToList(arr) {
  let l1 = null;
  for (let i = arr.length - 1; i >= 0; i--) {
    l1 = new ListNode(arr[i], l1);
  }
  return l1;
}

/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */
var addTwoNumbers = function (l1, l2) {
  return addNumber(l1, l2, 0);
};
// l1 = [9, 9, 9, 9, 9, 9, 9]
// l2 = [9, 9, 9, 9]
var addNumber = function (l1, l2, rest) {
  let sum = 0;
  let l4 = null;
  if (l1 !== null || l2 !== null) {
    if (l1 === null) sum = l2.val;
    else if (l2 === null) sum = l1.val;
    else sum = l1.val + l2.val;
    sum += rest;
    rest = sum >= 10 ? 1 : 0;
    l2 = l2 !== null ? l2.next : null;
    l1 = l1 !== null ? l1.next : null;
    l4 = new ListNode(sum % 10, addNumber(l1, l2, rest));
  } else if (rest > 0) l4 = new ListNode(rest, l4);
  return l4;
};

// I probably should implement this with another algorithm

let l1 = insertToList([9, 9, 9, 9, 9, 9, 9]);
let l2 = insertToList([9, 9, 9, 9]);

let l4 = addTwoNumbers(l1, l2);

console.log(JSON.stringify(l4));

function toarr(l4) {
  let arr = [];
  while (l4) {
    arr.push(l4.val);
    l4 = l4.next;
  }
  return arr;
}
console.log(toarr(l4));
